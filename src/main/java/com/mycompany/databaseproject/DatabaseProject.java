/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author worac
 */
public class DatabaseProject {

    public static void main(String[] args) {
       Connection conn = null;
       String url = "jdbc:sqlite:dcoffee.db";
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been etablish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        } 
            
            String sql ="SELECT * FROM category";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                System.out.println(rs.getInt("category_id")+" " 
                        + rs.getString("category_name"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

            
            try {
            if(conn!=null){
                    conn.close();
            }
            } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }

